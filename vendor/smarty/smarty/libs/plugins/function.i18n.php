<?php
/* 
 * smarty plugin
 * ————————————————————- 
 * File:     function.i18n.php 
 * Type:     function 
 * Name:     Fields internationalization for WHMCS 
 * Purpose:  Returns only the language data surrounded by language tags 
 * ————————————————————- 
 */

use WHMCS\Database\Capsule;

function smarty_function_i18n ($params, &$smarty) {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/init.php");
    $language    = $params['lang'];
    $default     = $params['default'];
    $key         = md5($default);
    $res         = Capsule::table('tblconfiguration')->where('setting', 'Language')->first();
    $defaultlang = $res->value;
    $result      = Capsule::table('mod_i18n_lang')->get();
    foreach ($result as $row) {
        $langs[$row->lang] = $row->enabled;
    }
    if ($langs[$language] == 0) {
        return $default;
    } else {
        $query = Capsule::table('mod_i18n_data')->where('id', $key);
        if ($query->count() > 0) {
            $row          = $query->first();
            $translations = unserialize($row->data);
            return $translations[$language];
        } else {
            $translations[$defaultlang] = $default;
            foreach ($langs as $lang => $enabled) {
                if ($enabled == 1) {
                    $translations[$lang] = $default;
                }
            }
            Capsule::table('mod_i18n_data')->insert(['id' => $key, 'default' => $default, 'data' => serialize($translations), 'translated' => 0]);
            return $default;
        }
    }
}
